package by.verbitsky.util;

import by.verbitsky.numberutil.util.StringUtils;

import static java.util.Objects.isNull;

public class Utils {

    public static boolean isAllPositiveNumbers(String... str) {
        if (isNull(str) || str.length == 0) {
            return false;
        }
        for (String item : str) {
            if (!StringUtils.isPositiveNumber(item)) {
                return false;
            }
        }
        return true;
    }
}
